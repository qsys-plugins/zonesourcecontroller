-- 4 Way priority crossfade with ducker over master output
-- by Tom Bland
-- May 2021

--[[
  Notes:

  This could be made more efficient using the meters as a signal precence 
  detector rather than having an SPD component. It was initially developed 
  without meters, these were added at a later date and so are a "bolt-on".

  It would be nice to re-write this to have the channel count defined by 
  the properites.

  V1.6: Added default values
  V1.5: Option "both" for internal settings so that daisychain linkable.
  V1.4: Added Ducker Attack, Hold, Release properties.
  V1.3: Fixed auto upgrade
  V1.2: Added attenuators
  V1.1: Added force active to PGM

]]

Version = "1.6"

PluginInfo =
{
  Name = "Zone Source Controller",
  Version = Version,
  Id = "TBLAND.ZSC",
  Description = "[["..Version.." hold and release properties implemented]]",
  ShowDebug = false
}

blockCol = {0, 0x57, 0x96}

function GetPrettyName(props)
  return "Zone Source Controller V"..Version
end

function GetColor(props)
  return blockCol
end

function GetProperties()
  props = {}

  return props
end

function RectifyProperties(props)
  return props
end

function GetControls(props)
  ctrls = {}

  --SIGNAL PRECENCE SETTINGS
  table.insert(ctrls,{
    Name = "Crossfade Threshold",
    ControlType = "Knob",
    ControlUnit = "dB",
    Min = -100,
    Max = 0,
    DefaultValue = -60,
    PinStyle = "Both",
    UserPin = true
  })
  table.insert(ctrls,{
    Name = "Crossfade Hold",
    ControlType = "Knob",
    ControlUnit = "Float",
    Min = 0,
    Max = 10,
    DefaultValue = 1,
    PinStyle = "Both",
    UserPin = true
  })

  --DUCKER SETTINGS
  table.insert(ctrls,{
    Name = "Ducker Depth",
    ControlType = "Knob",
    ControlUnit = "dB",
    Min = 0,
    Max = 100,
    PinStyle = "Both",
    DefaultValue = 20,
    UserPin = true
  })
  table.insert(ctrls,{
    Name = "Ducker Threshold",
    ControlType = "Knob",
    ControlUnit = "dB",
    Min = -60,
    Max = 0,
    DefaultValue = -20,
    PinStyle = "Both",
    UserPin = true
  })
  table.insert(ctrls,{
    Name = "Ducker Attack",
    ControlType = "Knob",
    ControlUnit = "Float",
    Min = 0,
    Max = 5,
    DefaultValue = 0.1,
    PinStyle = "Both",
    UserPin = true
  })
  table.insert(ctrls,{
    Name = "Ducker Hold",
    ControlType = "Knob",
    ControlUnit = "Float",
    Min = 0,
    Max = 30,
    DefaultValue = 1,
    PinStyle = "Both",
    UserPin = true
  })
  table.insert(ctrls,{
    Name = "Ducker Release",
    ControlType = "Knob",
    ControlUnit = "Float",
    Min = 0,
    Max = 5,
    DefaultValue = 1,
    PinStyle = "Both",
    UserPin = true
  })

  --INPUT MUTES
  table.insert(ctrls,{
    Name = "Defeat BGM",
    ControlType = "Button",
    ButtonType = "Toggle",
    PinStyle = "Both",
    UserPin = true
  })
  table.insert(ctrls,{
    Name = "Defeat PGM",
    ControlType = "Button",
    ButtonType = "Toggle",
    PinStyle = "Both",
    UserPin = true
  })
  table.insert(ctrls,{
    Name = "Defeat CTP",
    ControlType = "Button",
    ButtonType = "Toggle",
    PinStyle = "Both",
    UserPin = true
  })
  table.insert(ctrls,{
    Name = "Defeat TEST",
    ControlType = "Button",
    ButtonType = "Toggle",
    PinStyle = "Both",
    UserPin = true
  })
  table.insert(ctrls,{
    Name = "Defeat PAGE",
    ControlType = "Button",
    ButtonType = "Toggle",
    PinStyle = "Both",
    UserPin = true
  })

  --MIXER
  table.insert(ctrls,{
    Name = "BGM Volume",
    ControlType = "Knob",
    ControlUnit = "dB",
    Min = -60,
    Max = 0,
    Value = 0,
    PinStyle = "Both",
    UserPin = true
  })
  table.insert(ctrls,{
    Name = "PGM Volume",
    ControlType = "Knob",
    ControlUnit = "dB",
    Min = -60,
    Max = 0,
    Value = 0,
    PinStyle = "Both",
    UserPin = true
  })
  table.insert(ctrls,{
    Name = "CTP Volume",
    ControlType = "Knob",
    ControlUnit = "dB",
    Min = -60,
    Max = 0,
    Value = 0,
    PinStyle = "Both",
    UserPin = true
  })
  table.insert(ctrls,{
    Name = "TEST Volume",
    ControlType = "Knob",
    ControlUnit = "dB",
    Min = -60,
    Max = 0,
    Value = 0,
    PinStyle = "Both",
    UserPin = true
  })
  table.insert(ctrls,{
    Name = "PAGE Volume",
    ControlType = "Knob",
    ControlUnit = "dB",
    Min = -60,
    Max = 0,
    Value = 0,
    PinStyle = "Both",
    UserPin = true
  })
  table.insert(ctrls,{
    Name = "OUT Volume",
    ControlType = "Knob",
    ControlUnit = "dB",
    Min = -60,
    Max = 0,
    Value = 0,
    PinStyle = "Both",
    UserPin = true
  })

  --Force Active
  table.insert(ctrls,{
    Name = "Force PGM Active",
    ControlType = "Button",
    ButtonType = "Toggle",
    PinStyle = "Both",
    UserPin = true
  })

  --OUTPUTS
  table.insert(ctrls,{
    Name = "BGM Active",
    ControlType = "Indicator",
    IndicatorType = "Led"
  })
  table.insert(ctrls,{
    Name = "PGM Active",
    ControlType = "Indicator",
    IndicatorType = "Led"
  })
  table.insert(ctrls,{
    Name = "CTP Active",
    ControlType = "Indicator",
    IndicatorType = "Led"
  })
  table.insert(ctrls,{
    Name = "TEST Active",
    ControlType = "Indicator",
    IndicatorType = "Led"
  })
  table.insert(ctrls,{
    Name = "PAGE Active",
    ControlType = "Indicator",
    IndicatorType = "Led"
  })

  --METERS
  table.insert(ctrls,{
    Name = "BGM Level",
    ControlType = "Indicator",
    IndicatorType = "Meter"
  })
  table.insert(ctrls,{
    Name = "PGM Level",
    ControlType = "Indicator",
    IndicatorType = "Meter"
  })
  table.insert(ctrls,{
    Name = "CTP Level",
    ControlType = "Indicator",
    IndicatorType = "Meter"
  })
  table.insert(ctrls,{
    Name = "TEST Level",
    ControlType = "Indicator",
    IndicatorType = "Meter"
  })
  table.insert(ctrls,{
    Name = "PAGE Level",
    ControlType = "Indicator",
    IndicatorType = "Meter"
  })
  
  table.insert(ctrls,{
    Name = "OUT Level",
    ControlType = "Indicator",
    IndicatorType = "Meter"
  })

  return ctrls
end

function GetPins(props)
  local pins = {}

  table.insert(pins,{
    Name = "BGM",
    Direction = "input"
  })
  table.insert(pins,{
    Name = "PGM",
    Direction = "input"
  })
  table.insert(pins,{
    Name = "CTP",
    Direction = "input"
  })
  table.insert(pins,{
    Name = "TEST",
    Direction = "input"
  })
  table.insert(pins,{
    Name = "PAGE",
    Direction = "input"
  })
  table.insert(pins,{
    Name = "OUT",
    Direction = "output"
  })

  return pins
end 

function GetComponents(props)
  local components = {}
  table.insert(components,{
    Name = "matrix",
    Type = "mixer",
    Properties =
    {
      ["n_inputs"] = 6,
      ["n_outputs"] = 2,
      ["crosspoint_gain_type"] = 1
    }
  })
  table.insert(components,{
    Name = "ducker",
    Type = "priority_ducker",
    Properties =
    {
      ["n_channels"] = 1
    }
  })
  table.insert(components,{
    Name = "spMeter",
    Type = "signal_presence",
    Properties =
    {
      ["multi_channel_type"] = 3,
      ["multi_channel_count"] = 5
    }
  })
  table.insert(components,{
    Name = "meter",
    Type = "meter2",
    Properties =
    {
      ["meter_type"] = 1,
      ["multi_channel_type"] = 3,
      ["multi_channel_count"] = 6
    }
  })
  return components
end

function GetWiring(props)
  local wiring = {}

  --SIGNAL PRECENCE INPUT
  table.insert(wiring,{
    "BGM",
    "spMeter Input 1"
  })
  table.insert(wiring,{
    "PGM",
    "spMeter Input 2"
  })
  table.insert(wiring,{
    "CTP",
    "spMeter Input 3"
  })
  table.insert(wiring,{
    "TEST",
    "spMeter Input 4"
  })
  table.insert(wiring,{
    "PAGE",
    "spMeter Input 5"
  })

  --MATRIX INPUT
  table.insert(wiring,{
    "BGM",
    "matrix Input 1"
  })
  table.insert(wiring,{
    "PGM",
    "matrix Input 2"
  })
  table.insert(wiring,{
    "CTP",
    "matrix Input 3"
  })
  table.insert(wiring,{
    "TEST",
    "matrix Input 4"
  })
  table.insert(wiring,{
    "PAGE",
    "matrix Input 5"
  })
  
  --MATRIX -> DUCKER
  table.insert(wiring,{
    "matrix Output 1",
    "ducker Input 1"
  })
  table.insert(wiring,{
    "matrix Output 2",
    "ducker Priority"
  })

  --DUCKER OUTPUT
  table.insert(wiring,{
    "ducker Output 1",
    "OUT"
  })

  --METER INPUT
  table.insert(wiring,{
    "BGM",
    "meter Input 1"
  })
  table.insert(wiring,{
    "PGM",
    "meter Input 2"
  })
  table.insert(wiring,{
    "CTP",
    "meter Input 3"
  })
  table.insert(wiring,{
    "TEST",
    "meter Input 4"
  })
  table.insert(wiring,{
    "PAGE",
    "meter Input 5"
  })
  table.insert(wiring,{
    "ducker Output 1",
    "meter Input 6"
  })

  return wiring
end

-- Note: Statically created graphics due to simplicity
function GetControlLayout(props)
  local layout = {}
  local graphics = {}

  --*** Plugin Functions Text ***
  table.insert(graphics,{
    Type = "Text",
    Text = "Defeat",
    Position = {6, 20},
    Size = {60, 22},
    FontSize = 12,
    HTextAlign = "Center",
    VTextAlign = "Center"
  })
  table.insert(graphics,{
    Type = "Text",
    Text = "Active",
    Position = {6, 42},
    Size = {60, 16},
    FontSize = 12,
    HTextAlign = "Center",
    VTextAlign = "Center"
  })
  table.insert(graphics,{
    Type = "Text",
    Text = "Input Level",
    Position = {6, 58},
    Size = {60, 112},
    FontSize = 12,
    HTextAlign = "Center",
    VTextAlign = "Center"
  })
  table.insert(graphics,{
    Type = "Text",
    Text = "BGM",
    Position = {66,170},
    Size = {36,22},
    FontSize = 12,
    HTextAlign = "Center",
    VTextAlign = "Center"
  })
  table.insert(graphics,{
    Type = "Text",
    Text = "PGM",
    Position = {102,170},
    Size = {36,22},
    FontSize = 12,
    HTextAlign = "Center",
    VTextAlign = "Center"
  })
  table.insert(graphics,{
    Type = "Text",
    Text = "CTP",
    Position = {138,170},
    Size = {36,22},
    FontSize = 12,
    HTextAlign = "Center",
    VTextAlign = "Center"
  })
  table.insert(graphics,{
    Type = "Text",
    Text = "TEST",
    Position = {174,170},
    Size = {36,22},
    FontSize = 12,
    HTextAlign = "Center",
    VTextAlign = "Center"
  })
  table.insert(graphics,{
    Type = "Text",
    Text = "PAGE",
    Position = {210,170},
    Size = {36,22},
    FontSize = 12,
    HTextAlign = "Center",
    VTextAlign = "Center"
  })
  table.insert(graphics,{
    Type = "Text",
    Text = "OUT",
    Position = {272,170},
    Size = {36,22},
    FontSize = 12,
    HTextAlign = "Center",
    VTextAlign = "Center"
  })

  --*** PLUGIN CONFIG TEXT ***
  table.insert(graphics,{
    Type = "text",
    Text = "Crossfade Threshold",
    Position = {339,12},
    Size = {58,36},
    FontSize = 11,
    HTextAlign = "Left",
    VTextAlign = "Center"
  })
  table.insert(graphics,{
    Type = "text",
    Text = "Crossfade Hold (s)",
    Position = {339,48},
    Size = {58,36},
    FontSize = 11,
    HTextAlign = "Left",
    VTextAlign = "Center"
  })
  table.insert(graphics,{
    Type = "text",
    Text = "Ducker Depth",
    Position = {339,84},
    Size = {58,36},
    FontSize = 11,
    HTextAlign = "Left",
    VTextAlign = "Center"
  })
  table.insert(graphics,{
    Type = "text",
    Text = "Ducker Threshold",
    Position = {339,120},
    Size = {58,36},
    FontSize = 11,
    HTextAlign = "Left",
    VTextAlign = "Center"
  })
  table.insert(graphics,{
    Type = "text",
    Text = "Ducker Attack (s)",
    Position = {339,156},
    Size = {58,36},
    FontSize = 11,
    HTextAlign = "Left",
    VTextAlign = "Center"
  })
  table.insert(graphics,{
    Type = "text",
    Text = "Ducker Hold (s)",
    Position = {339,192},
    Size = {58,36},
    FontSize = 11,
    HTextAlign = "Left",
    VTextAlign = "Center"
  })
  table.insert(graphics,{
    Type = "text",
    Text = "Ducker Rel. (s)",
    Position = {339,228},
    Size = {58,36},
    FontSize = 11,
    HTextAlign = "Left",
    VTextAlign = "Center"
  })
  table.insert(graphics,{
    Type = "text",
    Text = "Level",
    Position = {6,192},
    Size = {60,35},
    FontSize = 11,
    HTextAlign = "Center",
    VTextAlign = "Center"
  })
  table.insert(graphics,{
    Type = "text",
    Text = "Force Active",
    Position = {6,227},
    Size = {60,35},
    FontSize = 11,
    HTextAlign = "Center",
    VTextAlign = "Center"
  })

  --*** DESCRIPTION TEXT ***
  table.insert(graphics,{
    Type = "text",
    Text = "Zone Source Controller by Tom Bland. A 4 stage priority crossfade tool with ducker for paging input. Background content, programme content, call to prayer, test content, and paging inputs are available and crossfade/duck in that order of priority. Each input has a defeat option for disabling that channel.",
    Position = {6,276},
    Size = {436,23},
    FontSize = 6,
    HTextAlign = "Left",
    VTextAlign = "Center"
  })

  --*** BOXES ***
  table.insert(graphics,{
    Type = "GroupBox",
    Text = "",
    Position = {6,8},
    Size = {312,261},
    StrokeWidth = 1,
    CornerRadius = 8
  })
  table.insert(graphics,{
    Type = "GroupBox",
    Text = "",
    Position = {330,8},
    Size = {112,259},
    StrokeWidth = 1,
    CornerRadius = 8
  })

  --*** CONTROLS ***
  layout["Defeat BGM"] = {
    Style = "Button",
    ButtonStype = "Toggle",
    Position = {66,20},
    Size = {36,22},
    Color = {255,0,0},
    Margin = 2,
    Radius = 2
  }
  layout["Defeat PGM"] = {
    Style = "Button",
    ButtonStype = "Toggle",
    Position = {102,20},
    Size = {36,22},
    Color = {255,0,0},
    Margin = 2,
    Radius = 2
  }
  layout["Defeat CTP"] = {
    Style = "Button",
    ButtonStype = "Toggle",
    Position = {138,20},
    Size = {36,22},
    Color = {255,0,0},
    Margin = 2,
    Radius = 2
  }
  layout["Defeat TEST"] = {
    Style = "Button",
    ButtonStype = "Toggle",
    Position = {174,20},
    Size = {36,22},
    Color = {255,0,0},
    Margin = 2,
    Radius = 2
  }
  layout["Defeat PAGE"] = {
    Style = "Button",
    ButtonStype = "Toggle",
    Position = {210,20},
    Size = {36,22},
    Color = {255,0,0},
    Margin = 2,
    Radius = 2
  }
  layout["Force PGM Active"] = {
    Style = "Button",
    ButtonStype = "Toggle",
    Position = {102,227},
    Size = {36,35},
    Color = {0,255,0},
    Margin = 2,
    Radius = 2
  }

  
  layout["Crossfade Threshold"] = {
    Style = "Knob",
    Position = {397,12},
    Size = {36,36}
  }    
  layout["Crossfade Hold"] = {
    Style = "Knob",
    Position = {397,48},
    Size = {36,36}
  }     
  layout["Ducker Depth"] = {
    Style = "Knob",
    Position = {397,84},
    Size = {36,36}
  }    
  layout["Ducker Threshold"] = {
    Style = "Knob",
    Position = {397,120},
    Size = {36,36}
  }    
  layout["Ducker Attack"] = {
    Style = "Knob",
    Position = {397,156},
    Size = {36,36}
  }    
  layout["Ducker Hold"] = {
    Style = "Knob",
    Position = {397,192},
    Size = {36,36}
  }    
  layout["Ducker Release"] = {
    Style = "Knob",
    Position = {397,228},
    Size = {36,36}
  }

  --*** INDICATORS ***
  layout["BGM Active"] = {
    Style = "Led",
    Position = {76,42},
    Size = {16,16},
    Color = {0, 255, 0},
    Margin = 3,
      Padding = 0
  }
  layout["PGM Active"] = {
    Style = "Led",
    Position = {112,42},
    Size = {16,16},
    Color = {0, 255, 0},
    Margin = 3,
    Padding = 0
  }
  layout["CTP Active"] = {
    Style = "Led",
    Position = {148,42},
    Size = {16,16},
    Color = {0, 255, 0},
    Margin = 3,
    Padding = 0
  }
  layout["TEST Active"] = {
    Style = "Led",
    Position = {184,42},
    Size = {16,16},
    Color = {0, 255, 0},
    Margin = 3,
    Padding = 0
  }
  layout["PAGE Active"] = {
    Style = "Led",
    Position = {220,42},
    Size = {16,16},
    Color = {0, 255, 0},
    Margin = 3,
    Padding = 0
  }

  layout["BGM Level"] = {
    Style = "Meter",
    MeterStyle = "Level",
    Position = {66,58},
    Size = {36,112},
    Margin = 0,
    CornerRadius = 3
  }
  layout["PGM Level"] = {
    Style = "Meter",
    MeterStyle = "Level",
    Position = {102,58},
    Size = {36,112},
    Margin = 0,
    CornerRadius = 3
  }
  layout["CTP Level"] = {
    Style = "Meter",
    MeterStyle = "Level",
    Position = {138,58},
    Size = {36,112},
    Margin = 0,
    CornerRadius = 3
  }
  layout["TEST Level"] = {
    Style = "Meter",
    MeterStyle = "Level",
    Position = {174,58},
    Size = {36,112},
    Margin = 0,
    CornerRadius = 3
  }
  layout["PAGE Level"] = {
    Style = "Meter",
    MeterStyle = "Level",
    Position = {210,58},
    Size = {36,112},
    Margin = 0,
    CornerRadius = 3
  }
  layout["OUT Level"] = {
    Style = "Meter",
    MeterStyle = "Level",
    Position = {272,58},
    Size = {36,112},
    Margin = 0,
    CornerRadius = 3
  }
  layout["BGM Volume"] = {
    Style = "Knob",
    Position = {66,192},
    Size = {36,36}
  }
  layout["PGM Volume"] = {
    Style = "Knob",
    Position = {102,192},
    Size = {36,36}
  }   
  layout["CTP Volume"] = {
    Style = "Knob",
    Position = {138,192},
    Size = {36,36}
  }      
  layout["TEST Volume"] = {
    Style = "Knob",
    Position = {174,192},
    Size = {36,36}
  }  
  layout["PAGE Volume"] = {
    Style = "Knob",
    Position = {210,192},
    Size = {36,36}
  } 
  layout["OUT Volume"] = {
    Style = "Knob",
    Position = {272,192},
    Size = {36,36}
  } 

  return layout, graphics
end

--*** CONSTANTS ***
BGM  = 1
PGM  = 2
CTP  = 3
TEST = 4
PAGE = 5
NOTHING = 6

--*** FUNCTIONAL/LOGIC CODE (does not require controls to be active)***
ActiveSignal = NOTHING

--*** RUNTIME CODE ***
if Controls then
  function updateActiveText()
    if ActiveSignal == BGM then 
      Controls["BGM Active"].Value = 1
      Controls["PGM Active"].Value = 0
      Controls["CTP Active"].Value = 0
      Controls["TEST Active"].Value = 0
    elseif ActiveSignal == PGM then 
      Controls["BGM Active"].Value = 0
      Controls["PGM Active"].Value = 1
      Controls["CTP Active"].Value = 0
      Controls["TEST Active"].Value = 0
    elseif ActiveSignal == CTP then 
      Controls["BGM Active"].Value = 0
      Controls["PGM Active"].Value = 0
      Controls["CTP Active"].Value = 1
      Controls["TEST Active"].Value = 0
    elseif ActiveSignal == TEST then 
      Controls["BGM Active"].Value = 0
      Controls["PGM Active"].Value = 0
      Controls["CTP Active"].Value = 0
      Controls["TEST Active"].Value = 1
    elseif ActiveSignal == NONE then 
      Controls["BGM Active"].Value = 0
      Controls["PGM Active"].Value = 0
      Controls["CTP Active"].Value = 0
      Controls["TEST Active"].Value = 0
    else 
      Controls["BGM Active"].Value = 0
      Controls["PGM Active"].Value = 0
      Controls["CTP Active"].Value = 0
      Controls["TEST Active"].Value = 0
    end
  end

  function changeActiveSignal(changeTo)
    --Duck amount. This would mean that BGM could be ducked by "show mode". I think this should be another plugin. Multi-priority ducker with force active and multiple ducker depths.
    matrix["input."..ActiveSignal.. ".output.1.gain"].Value = -100
    matrix["input."..changeTo.. ".output.1.gain"].Value = 0
    ActiveSignal = changeTo
    updateActiveText()
  end

  function updateActiveSignal()
    changeTo = NOTHING
    if     spMeter["signal.presence."..TEST].Value == 1 and Controls["Defeat TEST"].Value == 0 then
      changeTo = TEST
    elseif spMeter["signal.presence."..CTP].Value  == 1 and Controls["Defeat CTP"].Value  == 0 then 
      changeTo = CTP
    elseif (spMeter["signal.presence."..PGM].Value  == 1 or Controls["Force PGM Active"].Value == 1) and Controls["Defeat PGM"].Value  == 0 then 
      changeTo = PGM
    elseif spMeter["signal.presence."..BGM].Value  == 1 and Controls["Defeat BGM"].Value  == 0 then 
      changeTo = BGM
    else 
      changeTo = NOTHING 
    end

    if changeTo ~= ActiveSignal then
      print("Current = "..ActiveSignal.." change to: "..changeTo)
      changeActiveSignal(changeTo)
    end
  end

  --*** Initalise Indicators ***
  Controls["BGM Active"].Value = 0
  Controls["PGM Active"].Value = 0
  Controls["CTP Active"].Value = 0
  Controls["TEST Active"].Value = 0
  Controls["PAGE Active"].Value = 0

  --*** Initialise Matrix ***
  matrix["input."..BGM..".output.1.gain"].RampTime = 1
  matrix["input."..PGM..".output.1.gain"].RampTime = 1
  matrix["input."..CTP..".output.1.gain"].RampTime = 1
  matrix["input."..TEST..".output.1.gain"].RampTime = 1

  matrix["input."..BGM.. ".output.1.gain"].Value = -100
  matrix["input."..PGM.. ".output.1.gain"].Value = -100
  matrix["input."..CTP.. ".output.1.gain"].Value = -100
  matrix["input."..TEST..".output.1.gain"].Value = -100
  matrix["input."..PAGE..".output.1.gain"].Value = -100

  matrix["input."..BGM.. ".output.2.gain"].Value = -100
  matrix["input."..PGM.. ".output.2.gain"].Value = -100
  matrix["input."..CTP.. ".output.2.gain"].Value = -100
  matrix["input."..TEST..".output.2.gain"].Value = -100
  matrix["input."..PAGE..".output.2.gain"].Value = 0

  --*** Initialise ducker ***
  ducker["depth"].Value = Controls["Ducker Depth"].Value
  Controls["Ducker Depth"].EventHandler = function(ctl)
    ducker["depth"].Value = ctl.Value
  end
  ducker["threshold.level"].Value = Controls["Ducker Threshold"].Value
  Controls["Ducker Threshold"].EventHandler = function(ctl)
    ducker["threshold.level"].Value = ctl.Value
  end
  ducker["attack"].Value = Controls["Ducker Attack"].Value
  Controls["Ducker Attack"].EventHandler = function(ctl)
    ducker["attack"].Value = ctl.Value
  end
  ducker["hold.time"].Value = Controls["Ducker Hold"].Value
  Controls["Ducker Hold"].EventHandler = function(ctl)
    ducker["hold.time"].Value = ctl.Value
  end
  ducker["release"].Value = Controls["Ducker Release"].Value
  Controls["Ducker Release"].EventHandler = function(ctl)
    ducker["release"].Value = ctl.Value
  end

  --*** Initialise signal precence detector ***
  spMeter["threshold"].Value = Controls["Crossfade Threshold"].Value
  Controls["Crossfade Threshold"].EventHandler = function()
    spMeter["threshold"].Value = Controls["Crossfade Threshold"].Value
  end
  spMeter["hold.time"].Value = Controls["Crossfade Hold"].Value
  Controls["Crossfade Hold"].EventHandler = function(ctl)
    spMeter["hold.time"].Value = ctl.Value
  end

  spMeter["signal.presence."..BGM].EventHandler = function(ctl)
    if ctl.Value == 1 then 
      print("BGM ACTIVE")
    end
    updateActiveSignal()
  end
  spMeter["signal.presence."..PGM].EventHandler = function(ctl)
    if ctl.Value == 1 then 
      print("PGM ACTIVE")
    end
    updateActiveSignal()
  end
  spMeter["signal.presence."..CTP].EventHandler = function(ctl)
    if ctl.Value == 1 then 
      print("CTP ACTIVE")
    end
    updateActiveSignal()
  end
  spMeter["signal.presence."..TEST].EventHandler = function(ctl)
    if ctl.Value == 1 then 
      print("TEST ACTIVE")
    end
    updateActiveSignal()
  end
  spMeter["signal.presence."..PAGE].EventHandler = function(ctl)
    if ctl.Value == 1 then 
      print("PAGE ACTIVE")
      Controls["PAGE Active"].Value = 1
    else
      Controls["PAGE Active"].Value = 0
    end
    --updateActiveSignal()
  end

  --*** Configure Input Defeats ***
  matrix["input."..BGM..".mute"].Value = Controls["Defeat BGM"].Value
  Controls["Defeat BGM"].EventHandler = function()
    matrix["input."..BGM..".mute"].Value = Controls["Defeat BGM"].Value
    updateActiveSignal()
  end

  matrix["input."..PGM..".mute"].Value = Controls["Defeat PGM"].Value
  Controls["Defeat PGM"].EventHandler = function()
    matrix["input."..PGM..".mute"].Value = Controls["Defeat PGM"].Value
    updateActiveSignal()
  end

  matrix["input."..CTP..".mute"].Value = Controls["Defeat CTP"].Value
  Controls["Defeat CTP"].EventHandler = function()
    matrix["input."..CTP..".mute"].Value = Controls["Defeat CTP"].Value
    updateActiveSignal()
  end

  matrix["input."..TEST..".mute"].Value = Controls["Defeat TEST"].Value
  Controls["Defeat TEST"].EventHandler = function()
    matrix["input."..TEST..".mute"].Value = Controls["Defeat TEST"].Value
    updateActiveSignal()
  end

  matrix["input."..PAGE..".mute"].Value = Controls["Defeat PAGE"].Value
  Controls["Defeat PAGE"].EventHandler = function()
    matrix["input."..PAGE..".mute"].Value = Controls["Defeat PAGE"].Value
    updateActiveSignal()
  end

  --*** Configure Volume Controls ***
  Controls["BGM Volume"].EventHandler = function()
    matrix["input."..BGM..".gain"].Value = Controls["BGM Volume"].Value
  end
  Controls["PGM Volume"].EventHandler = function()
    matrix["input."..PGM..".gain"].Value = Controls["PGM Volume"].Value
  end
  Controls["CTP Volume"].EventHandler = function()
    matrix["input."..CTP..".gain"].Value = Controls["CTP Volume"].Value
  end
  Controls["TEST Volume"].EventHandler = function()
    matrix["input."..TEST..".gain"].Value = Controls["TEST Volume"].Value
  end
  Controls["PAGE Volume"].EventHandler = function()
    matrix["input."..PAGE..".gain"].Value = Controls["PAGE Volume"].Value
  end
  Controls["OUT Volume"].EventHandler = function()
    ducker["output.gain"].Value = Controls["OUT Volume"].Value
  end

  --*** Configure Force Active ***
  Controls["Force PGM Active"].EventHandler = function()
    updateActiveSignal()
  end

  --*** Update Meters ***
  meterRefresh = Timer.New()
  meterRefresh.EventHandler = function()
    Controls["BGM Level"].Value = meter["meter.1"].Value
    Controls["PGM Level"].Value = meter["meter.2"].Value
    Controls["CTP Level"].Value = meter["meter.3"].Value
    Controls["TEST Level"].Value = meter["meter.4"].Value
    Controls["PAGE Level"].Value = meter["meter.5"].Value
    Controls["OUT Level"].Value = meter["meter.6"].Value
  end
  meterRefresh:Start(0.2)

  --*** initialise state ***
  updateActiveSignal()
end