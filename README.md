# Zone Source Controller #

Zone Source Controller is a 4 stage priority crossfader with ducker for paging input.
From lowest to highest priority:

1.	Background content (BGM)
2.	Programme content (PGM)
3.	Call to prayer (CTP)
4.	Test content (TEST)

The Paging input ducks the master output.

Each input has a defeat option for disabling that channel.

### Disclaimer ###

This plugin is provided as my first contribution to the QSC Communities Code Exchange (developers.qsc.com). It is provided as-is, with no gaurentee of support, or future compatibility.